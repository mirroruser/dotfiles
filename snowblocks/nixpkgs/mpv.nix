{ pkgs, ... }:

{
  xdg = {
    configFile = {
      "mpv/config" = {
        source = mpv/config;
      };
    };
  };
}
