<div align="center">
  <p>Dotfile home</p>

  <p>
    <img src="https://assets-cdn.github.com/favicon.ico" width=24 height=24 />
    <a href="https://github.com/eadwu/dotfiles/releases/latest">
      <img src="https://img.shields.io/github/release/eadwu/dotfiles.svg" />
    </a>
    <a href="https://github.com/arcticicestudio/snowsaw/tree/develop">
      <img src="https://img.shields.io/badge/snowsaw-prerelease-88C0D0.svg" />
    </a>
    <a href="https://www.archlinux.org">
      <img src="https://img.shields.io/badge/OS-Arch_Linux-1793D1.svg" />
    </a>
  </p>

  <p>Powered by
    <br />
    <a href="https://github.com/arcticicestudio/snowsaw">
      <img src="https://cdn.rawgit.com/arcticicestudio/snowsaw/develop/assets/snowsaw-logo-banner.svg" />
    </a>
  </p>
</div>

---

<div align="center">
  <p>Copyright &copy; 2018 Edmund Wu</p>

  <p>
    <a href="https://gitlab.com/eadwu/dotfiles/blob/master/LICENSE">
      <img src="https://img.shields.io/badge/License-BSD--3--Clause-5E81AC.svg" />
    </a>
  </p>
</div>
